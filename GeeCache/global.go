package GeeCache

import "sync"

var (
	clientsGetter INodeClientGetter
	groupsRwMutex sync.RWMutex
	groups        = make(map[string]IGroup)
	hostBaseUrl   string
)

const (
	binaryContentType = "application/octet-stream"
)
